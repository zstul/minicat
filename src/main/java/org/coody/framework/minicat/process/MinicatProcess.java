package org.coody.framework.minicat.process;

import org.coody.framework.minicat.builder.iface.HttpBuilder;
import org.coody.framework.minicat.container.ServletContainer;
import org.coody.framework.minicat.entity.ApplicationFilterChain;
import org.coody.framework.minicat.servlet.HttpServlet;
import org.coody.framework.minicat.threadpool.MiniCatThreadPool;

public class MinicatProcess {

	static long num = 0;

	public static void doService(HttpBuilder build) throws 	Exception {
			HttpServlet servlet = ServletContainer.getServlet(build.getRequest().getRequestURI());
			ApplicationFilterChain chain=new ApplicationFilterChain(servlet);
			chain.doFilter(build.getRequest(), build.getResponse());
			num++;
		
	}

	static {
		MiniCatThreadPool.MINICAT_POOL.execute(new Runnable() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					long currentNum = num;
					num = 0;
					System.out.println("QPS:" + currentNum);

				}
			}
		});

	}
}
